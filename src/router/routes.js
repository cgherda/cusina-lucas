
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '',  name: 'home', component: () => import('pages/Index.vue') },
      { path: '/menu', name: 'menu', component: () => import('pages/Menu.vue') },
      { path: '/cart', name: 'cart', component: () => import('pages/Cart.vue') },
      { path: '/success', name: 'thankyou', component: () => import('pages/ThankYou.vue') },
      { path: '/login', name: 'lucasLogin', component: () => import('pages/Login.vue') },
      { path: '/orders', meta: { requiresAuth: true }, name: 'order', component: () => import('pages/Orders.vue') },
      { path: '/assistant', meta: { requiresAuth: true }, name: 'assistant', component: () => import('pages/Assistant.vue') },
      { path: '/menu-manage', meta: { requiresAuth: true }, name: 'menuManage', component: () => import('pages/MenuManage.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
